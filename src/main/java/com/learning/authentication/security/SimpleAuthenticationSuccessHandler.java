package com.learning.authentication.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.authentication.common.JWTProvider;
import com.learning.authentication.security.dto.AuthResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 認証が成功した時の処理
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final JWTProvider provider;

    private final ObjectMapper objectMapper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
        throws IOException {
        if (res.isCommitted()) {
            log.info("Response has already been committed.");
            return;
        }

        // HTTPステータス設定
        res.setStatus(HttpStatus.OK.value());

        // Body設定
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
        res.setCharacterEncoding(StandardCharsets.UTF_8.name());
        PrintWriter out = res.getWriter();
        out.print(this.objectMapper.writeValueAsString(
            new AuthResponse(
                this.provider.createToken(req, auth)
            )
        ));
        out.flush();
        out.close();
    }
}