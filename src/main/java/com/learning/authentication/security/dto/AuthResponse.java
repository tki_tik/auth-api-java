package com.learning.authentication.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class AuthResponse {

    /** JWTトークン */
    private String token;
}