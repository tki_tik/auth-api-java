package com.learning.authentication.application.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "prelogin")
@Slf4j
public class PreLoginController {

    /**
     * ログイン前処理。
     * 
     * @param req リクエスト情報
     * @return トークン
     */
    @GetMapping
    public String preLogin(HttpServletRequest req) {
        log.info("reqest : " + req.toString());
        DefaultCsrfToken token = (DefaultCsrfToken) req.getAttribute("_csrf");
        if (token == null) {
            throw new RuntimeException("could not get a token.");
        }
        return token.getToken();
    }
}