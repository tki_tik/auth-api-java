package com.learning.authentication.common;

public class Utility {

    /**
     * コンストラクタ。
     * インスタンス化させない為private。
     */
    private Utility() {
    }

    /** 権限 */
    public static enum Authority {
        ROLE_USER,
        ROLE_ADMIN
    };

    /**
     * AuthorityのEnum型変換。
     *
     * @param str 変換前文字列
     * @return 変換後Enum 対象が存在しなければnull
     */
    public static Authority convertAuthorityEnum(String str) {
        Authority[] enumArray = Authority.values();

        for (Authority enumStr : enumArray) {
            if (enumStr.toString().equals(str)) {
                return enumStr;
            }
        }
        return null;
    }
}