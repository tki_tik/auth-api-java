# 認証用RestAPI

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

**利用者用**

- [概要](#%E6%A6%82%E8%A6%81)
- [IF定義](#if%E5%AE%9A%E7%BE%A9)
    - [リクエスト項目](#%E3%83%AA%E3%82%AF%E3%82%A8%E3%82%B9%E3%83%88%E9%A0%85%E7%9B%AE)
    - [レスポンス項目](#%E3%83%AC%E3%82%B9%E3%83%9D%E3%83%B3%E3%82%B9%E9%A0%85%E7%9B%AE)
        - [Payloadの構成](#payload%E3%81%AE%E6%A7%8B%E6%88%90)

**開発者用**
- [本プロジェクトに関して](#%E6%9C%AC%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%81%AB%E9%96%A2%E3%81%97%E3%81%A6)
- [事前準備](#%E4%BA%8B%E5%89%8D%E6%BA%96%E5%82%99)
    - [プロジェクトクローン](#%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%82%AF%E3%83%AD%E3%83%BC%E3%83%B3)
    - [Homebrewインストール](#homebrew%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [yarnインストール](#yarn%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [node_modulesインストール](#node_modules%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
- [使い方](#%E4%BD%BF%E3%81%84%E6%96%B9)
    - [サーバ起動](#%E3%82%B5%E3%83%BC%E3%83%90%E8%B5%B7%E5%8B%95)
    - [動作確認（UT）](#%E5%8B%95%E4%BD%9C%E7%A2%BA%E8%AA%8Dut)
    - [動作確認（heroku）](#%E5%8B%95%E4%BD%9C%E7%A2%BA%E8%AA%8Dheroku)
    - [DB情報](#db%E6%83%85%E5%A0%B1)
    - [テーブル定義](#%E3%83%86%E3%83%BC%E3%83%96%E3%83%AB%E5%AE%9A%E7%BE%A9)
        - [ユーザ情報](#%E3%83%A6%E3%83%BC%E3%82%B6%E6%83%85%E5%A0%B1)
        - [ユーザ詳細](#%E3%83%A6%E3%83%BC%E3%82%B6%E8%A9%B3%E7%B4%B0)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## 概要

認証用API。
本プロジェクトには下記機能が含まれています。
 - 認証機能（Spring Security）


## IF定義

|  メソッド  |  エンドポイントURL  |  コンテンツタイプ  |
| ---- | ---- | ---- |
| POST | https://auth-api-j.herokuapp.com/login | application/x-www-form-urlencoded |

### リクエスト項目

|  項目  |  内容  |  桁数  |  説明  |
| ---- | ---- | :---: | ---- |
| id | ログインID | 6 | テストユーザ（管理者）："999999"<br>テストユーザ（ユーザ）："000001" |
| pass | パスワード | 1...255 | テストユーザ（管理者）："admin"<br>テストユーザ（ユーザ）："user" |

### レスポンス項目

|  処理結果  |  説明  |
| ---- | ---- |
| 200 | ログイン成功時（セッションの生成、CSRFトークンの生成） |
| 401 | ログイン失敗時（ログインID間違い、パスワード間違い） |

|  ヘッダー項目  |  内容  |  説明  |
| ---- | ---- | ---- |
| X-AUTH-TOKEN | 認証トークン | 認証管理用JWT |

#### Payloadの構成


|  項目  |  内容  |  説明  |
| ---- | ---- | ---- |
| sub | Subject | JWTのユニークなキー |
| username | ユーザ名 | ログイン者名 |
| role | 権限 | 管理者："ROLE_ADMIN"<br>ユーザ："ROLE_USER" |
| iat | 発行時間 | JWTが発行された日時 |
| exp | 満了時間 | JWTが失効する日時 |

<br>
<br>


## ※開発者用


## 本プロジェクトに関して

本プロジェクトはMac OS XまたはLinuxで使用することを目的としています。
また、以下の手順はMac OS X専用です。


## 事前準備

### プロジェクトクローン

```bash
$ git clone https://gitlab.com/taiki-self-learning/auth-rest-api.git 
```

### Homebrewインストール

```bash
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"  
```

### yarnインストール

```bash
$ brew install yarn --ignore-dependencies
```

### node_modulesインストール

```bash
$ yarn install
```

## 使い方

### サーバ起動

```bash
$ cd 本プロジェクトパス/authentication-e2e
$ yarn start
```

### 動作確認（UT）

```bash
$ cd 本プロジェクトパス/authentication-e2e
$ yarn test:login:ut
```

### 動作確認（heroku）

```bash
$ cd 本プロジェクトパス/authentication-e2e
$ yarn test:login:heroku
```

### DB情報

|  DB  |  ホスト名  |  ポート  |
| :----: | :----: | :----: |
| PostgreSQL13.2 | ec2&#45;34&#45;206&#45;8&#45;52&#046;compute&#45;1&#046;amazonaws&#046;com | 5432 |

|  データベース名  |  ユーザ名  |  パスワード  |
| :----: | :----: | :----: |
| ddm7nv54jp34c8 | bsoddfupimtvuy | bc27e94b8c708f31bd7d60c8dce4bd3e9cdee9ff972641a0f5f6d68ba9ac9adc |

### テーブル定義

#### ユーザ情報

|  列名<br>（論理名）  |  列名<br>（物理名）  |  データ型  |  長さ  |  PK  |  NOT <br>NULL  |  備考  |
| ---- | ---- | :----: | :----: | :----: | :----: | ---- |
| ユーザID | id | varchar | 6 | 1 | ◯ |  |
| 権限 | authority | varchar | 64 |  | ◯ |  |
| 作成日時 | created_at | timestamp |  |  |  |  |
| 更新日時 | updated_at | timestamp |  |  |  |  |

#### ユーザ詳細

|  列名<br>（論理名）  |  列名<br>（物理名）  |  データ型  |  長さ  |  PK  |  NOT <br>NULL  |  備考  |
| ---- | ---- | :----: | :----: | :----: | :----: | ---- |
| ユーザID | user_id | varchar | 6 | 1 | ◯ |  |
| ユーザ名 | user_name | varchar | 64 |  | ◯ |  |
| パスワード  | password | varchar | 255 |  | ◯ | BCryptPasswordEncoder.encodeハッシュ化 |
| Eメール | email | varchar | 64 |  | ◯ |  |
| 作成日時 | created_at | timestamp |  |  |  |  |
| 更新日時 | updated_at | timestamp |  |  |  |  |
